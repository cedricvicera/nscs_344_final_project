clear
rattime = [];
antitime= [];
irtime = [];
H=90;
trials = 100;
tmax = 720;
for i = 1:trials
    rattime(i)=rational(H, .3, .25, tmax);
    antitime(i)=antirat(H, .3, .25, tmax);
    irtime(i) = irrat(H, .3, .25, tmax);
end
edges = 0:30:tmax;
subplot(3,1,1)
histogram(rattime, edges)
xticks(0:60:tmax);
xlim([0 tmax]);
ylim([0 trials]);
title('Rational Actor Bankruptcy')
xlabel('days until bankruptcy')
ylabel('# of trials')

subplot(3,1,2)
histogram(antitime, edges)
xticks(0:60:tmax);
xlim([0 tmax]);
ylim([0 trials]);
title('Anti-Rational Actor Bankruptcy')
xlabel('days until bankruptcy')
ylabel('# of trials')

subplot(3,1,3)
histogram(irtime, edges)
xticks(0:60:tmax);
xlim([0 tmax]);
ylim([0 trials]);
title('Irrational Actor Bankruptcy')
xlabel('days until bankruptcy')
ylabel('# of trials')

function t=rational(H,sig0,M0,tmax)
R = []; %available resources
R(1) = 100; % percent resources available
sig= [];
sig(1) = sig0;
S = [];%daily spending
M = []; %real mu values
t=1;
while R(t) > 0 && t < tmax
    if t == 1
        M(t) = M0;
    else
       M(t) = mean(S);
    end
    if rem(t,365) == 0
        R(t)=R(t)+100;
    end
    stress(t) = stress_gen(R(t), sig, H, M(t));
    daily = abs(M(t)+rand*sig);
    if stress(t) == 0
        S(t) = daily;
    else
        S(t) = daily-(daily*stress(t));
    end
    R(t+1)= R(t) - S(t);
    t=t+1;
    
end
end

function t=antirat(H,sig0,M0,tmax)
R = []; %available resources
R(1) = 100; % percent resources available
sig= [];
sig(1) = sig0;
S = [];%daily spending
M = []; %real mu values
t=1;
while R(t) > 0 && t < tmax
    if t == 1
        M(t) = M0;
    else
        M(t) = mean(S);
    end
    if rem(t,365) == 0
        R(t)=R(t)+100;
    end
    if t==1
        sig(t)=sig0;
    else
        sig(t)=sig0+stress(t-1);
    end
    stress(t) = stress_gen(R(t), sig(t), H, M(t));
    S(t) = abs(M(t) + randn*sig(t));
    R(t+1)= R(t) - S(t);
    t=t+1;
    
end
end


function t=irrat(H,sig0,M0,tmax)
R = []; %available resources
R(1) = 100; % percent resources available
sig= [];
sig(1) = sig0;
S = [];%daily spending
M = []; %real mu values
t=1;
while R(t) > 0 && t < tmax
    if t == 1
        M(t) = M0;
    else
        M(t) = mean(S);
   end
    if rem(t,365) == 0
        R(t)=R(t)+100;
    end
    stress(t) = stress_gen(R(t), sig, H, M(t));
    
    S(t) = abs(M(t) + randn*sig);

    R(t+1)= R(t) - S(t);
    t=t+1;
    
end
end
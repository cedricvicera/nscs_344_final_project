clear
%attempt to model how long it takes to go bankrupt given a value of sigma
%and a starting R( resources)
R = []; %available resources
H = 90; %planning x amount of days into the future
R(1) = 100; % percent resources available
sig = .3;
S = [];%daily spending
M0 = .25;%average daily spending (CONSTANT)
M = []; %real mu values
t=1;
while R(t) > 0 && t < 7300
    if t == 1
        M(t) = M0;
    else
       M(t) = mean(S);
    end
    if rem(t,365) == 0
        R(t)=R(t)+100;
    end
    stress(t) = stress_gen(R(t), sig, H, M(t));
    daily = abs(M(t)+rand*sig);
    if stress(t) == 0
        S(t) = daily;
    else
        S(t) = daily-(daily*stress(t));
    end
    R(t+1)= R(t) - S(t);
    t=t+1;
    
end

time = 1:t;

figure(1);
clf;

subplot(2,1,1);
plot(stress);
title('Stress');
xlabel('time (days)');
ylabel('stress');
legend('stress');

subplot(2,1,2);
plot(R);
title('Resources');
xlabel('time (days)');
ylabel('resources');
legend('resources');


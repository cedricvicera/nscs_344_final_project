function pBank = stress_gen(R, sig, H, M)
s=sqrt(H)*sig; %s is standard variance 

m = 1/(sqrt(2*pi)*s); %multiple 
R2 = [-100:0.1:100]; % scale to size
d = 2*(s^2); %denominator

for i = 1:length(R2)
    n = (R2(i) - (R - H * M))^2; %numerator
    p(i) =  m*exp(-(n/d));
end
if sum(p)== 0
    pBank = 0;
else
pBank = sum(p(R2<0)) / sum(p); % probability of going bankrupt
end
end